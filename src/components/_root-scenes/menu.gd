extends Control


func _ready():
	get_node('StartButton').connect(
		'pressed', self, '__on_start'
	)


func __on_start():
	SCENE.goto_scene('world')
